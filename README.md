# Core ABNF Extensions with Vim Plugin

⚠️ **Warning: consider using [PEG](https://github.com/pointlander/peg) instead of ABNF when possible.**

ABNF is one metalanguage for describing specifications and patterns --- particularly when dealing with network protocols. This repo contains the [extended core rules](extended-core.abnf) used to extend the core built-in rules for the current ABNF specification particularly to take into account the full Unicode character set. 

## Vim Plugin

This repo can also be used to easily add Vim syntax highlighting for ABNF and these core rules using the `Plug` plugin manager (which has also been [included](autoload/plug.vim) for convenience but will need to be put in your `.vim/autoload` directory). If you don't have the solarized and pandoc plugins you should get them as well:

```
call plug#begin('~/.vim/plugged')
Plug 'https://github.com/robmuh/abnf'
call plug#end()
```

## Related Work

These rules are fundamentally distilled from the exceptional and current [Go `unicode` package](https://golang.org/pkg/unicode/#IsPrint) (currently at version 11.0.0). Other influences include:

* [Internationalized Resource Identifiers](https://tools.ietf.org/html/rfc3987)
* [Unicode ABNF Extension](https://tools.ietf.org/html/draft-seantek-unicode-in-abnf-03)

The latter is particularly unusable simply because it focuses too much on the ranges and not on the more practical categorizations of individual code points.

## Note and Praise for Go (golang)

When considering the importance for proper handing of UNICODE the Go language objectively surpasses every other language currently in mainstream use. It's creators were substantially involved in the [definition of UNICODE](https://en.wikipedia.org/wiki/Universal_Coded_Character_Set). Applications that involve text and almost required to use Go to do so properly. Other languages (Haskell comes to mind) come particularly close to matching the comprehensive and extensive treatment of UNICODE that Go does but none reach or surpass it.
